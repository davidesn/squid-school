package school.management.system;

/**
 * This class is responsible for keeping the
 * track of students fees, name ,grade & fees
 * paid.
 */
public class Student {

    private int id;
    private String name;
    private int grade;
    private int PaidLoans;
    private int TotalLoans;

    /**
     * To create a new student by initializing.
     * @param id id for the student: unique.
     * @param name name of the student.
     * @param grade grade of the student.
     * Loans for every student are $30,000.
     * Loans paid initially are $0.
     */
    public Student(int id, String name, int grade) {
        this.id = id;
        this.name = name;
        this.grade = grade;
        this.PaidLoans = 0;
        this.TotalLoans = 30000;
    }

    public int getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Used to update the student's grade.
     * @param grade new grade of the student.
     */
    public void setGrade(int grade) {
        this.grade = grade;
    }

    public int getGrade() {
        return grade;
    }

    /**
     * Keep adding the fees to feesPaid Field.
     * Add the fees to the fees paid.
     * The school is going receive the funds.
     * @param amount of loans that the student pays.
     */
    public void payLoans(int amount){
        PaidLoans += amount;
        School.updateIncomes(PaidLoans);
    }

    public int getPaidLoans() {
        return PaidLoans;
    }

    public int getTotalLoans() {
        return TotalLoans;
    }

    @Override
    public String toString() {
        return "Student's name: " + name + ", id: " + id + ", total fees paid so far: $" + PaidLoans;
    }
}
