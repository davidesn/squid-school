package school.management.system;

/**
 * This class is responsible for keeping the track
 * of teacher's name, id, salary.
 */
public class Teacher {

    private int id;
    private String name;
    private int salary;
    private int balance;

    /**
     * Creates a new Teacher object.
     * @param id id for the teacher.
     * @param name name of the teacher.
     * @param salary salary of the teacher.
     */
    public Teacher(int id, String name, int salary) {
        this.id = id;
        this.name = name;
        this.salary = salary;
        this.balance = 0;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    //TODO: 1) Add setName, in order to edit existing teachers infos

    public int getSalary() {
        return  salary;
    }

    public void setSalary(int salary) {
        this.salary = salary;
    }

    /**
     * Updates the balance, in other words the total money received by the organization.
     * Removes from the total money earned by the school.
     * @param salary
     */
    public void payout(int salary) {
        balance += salary;
        School.updateExpenses(salary);
    }

    public int getBalance() {
        return  balance;
    }

    @Override
    public String toString() {
        return "Name of the Teacher: " + name
                +", id: " + id + ", salary: " + salary;
    }
}
