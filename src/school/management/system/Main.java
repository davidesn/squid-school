package school.management.system;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static School school = new School("Sollers Academy",
                                  new ArrayList<>(), new ArrayList<>());
    public static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        int input;

        while (true) {
            menu();
            input = Integer.parseInt(br.readLine());
            switch (input) {
                case 1: System.out.println(school.toString());
                        break;
                case 2: studentPortal(getStudent());
                        break;
                case 3: search();
                        break;
                case 4: //TODO: 5) Implement logic for adding a new student from keyboard
                        break;
                case 5: //TODO: 6) Call the implemented method
                        break;
                case 69: demo();
                        break;
                case 0: return;
                default: System.out.println("Invalid input");
                         break;
            }
        }

    }

    public static void studentPortal(Student student) throws IOException {
        int input;

        while (true) {
            auth_menu();
            input = Integer.parseInt(br.readLine());
            switch (input) {
                case 1: //TODO: 2) Implement logic
                        break;
                case 2: System.out.println("Remaining loan value: " +
                        (student.getTotalLoans() - student.getPaidLoans()));
                        break;
                case 3: System.out.println(student.getPaidLoans());
                        break;
                case 4: if ((student.getTotalLoans() - student.getPaidLoans()) <= 0) {
                            System.out.println("There's no taxes left to pay.");
                        } else {
                            System.out.println("Insert amount to pay: ");
                            student.payLoans(Integer.parseInt(br.readLine()));
                        }
                        break;
                case 0: return;
                default: System.out.println("Invalid input");
                        break;
            }
        }
    }

    private static Student getStudent() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Insert the student id: ");
        int studentId = Integer.parseInt(br.readLine());
        //TODO: 4) implement checks and feedback if student is not found

        for (Student student : school.getStudents()) {
            if (student.getId() == studentId) {
                return student;
            }
        }
        return null;
    }

    private static void search() throws IOException {
        System.out.println("Type 's' to search for students or 't' for teachers: ");
        char type = br.readLine().charAt(0);
        if (type == 's' || type == 't') {
            System.out.println("Insert a name: ");
            String name = br.readLine();

            if (type == 's') {
                List<Student> studentList = school.getStudents();
                studentList.forEach(student -> { if (student.getName().toLowerCase().contains(name.toLowerCase())) {
                    System.out.println(student.toString());}});
            } else {
                List<Teacher> studentList = school.getTeachers();
                studentList.forEach(teacher -> { if (teacher.getName().toLowerCase().contains(name.toLowerCase())) {
                    System.out.println(teacher.toString());}});
            }
        } else {
            System.out.println("Invalid input.");
        }
    }

    public static void menu() {
        System.out.println(
                "---------\n" +
                "-1: School details\n" +
                "-2: Student Portal\n" +
                "-3: Search...\n" +
                "-4: Add student\n" +           //TODO: 5) Implement logic
                "-5: Expel student\n" +         //TODO: 6) Implement logic
                "-69: Demo\n" +
                "-0: Quit\n");
    }

    public static void auth_menu() {
        System.out.println(
                "---------\n" +
                "-1: Change birth name\n" +     //TODO 2) Implement logic
                "-2: Tax summary\n" +
                "-3: History of Payments\n" +
                "-4: Pay Loans\n" +
                "-0: Back\n");
    }

    /**
     * Instanciates objects for Teacher and Student classes. Use this to quickly generate a dataset
     * if lazy or for testing purposes. NOTE: This method is a template and doesn't require QA.
     */
    public static void demo() {
        Teacher lizzy = new Teacher(1,"Lizzy",500);
        Teacher melissa = new Teacher(2,"Melissa",700);
        Teacher vanderhorn = new Teacher(3,"Vanderhorn",600);
        List<Teacher> teacherList = new ArrayList<>();
        teacherList.add(lizzy);
        teacherList.add(melissa);
        teacherList.add(vanderhorn);

        Student rose = new Student(1,"Rose Potter",4);
        Student michael = new Student(3,"Michael",5);
        Student mateusz = new Student(2,"Mateusz Ivanovich",12);
        List<Student> studentList = new ArrayList<>();
        studentList.add(rose);
        studentList.add(michael);
        studentList.add(mateusz);

        school =  new School("GHS", teacherList, studentList);
        Teacher megan = new Teacher(6,"Megan", 900);
        school.addTeacher(megan);
        Student martin = new Student (420, "Martin Gale", 5);
        school.addStudent(martin);

        rose.payLoans(5000);
        mateusz.payLoans(6000);
        System.out.println("GHS has earned $"+ school.getTotalMoneyEarned());
        System.out.println("------Making SCHOOL PAY SALARY----");
        lizzy.payout(lizzy.getSalary());
        System.out.println("GHS has spent for salary to " + lizzy.getName()
                +" and now has $" + school.getTotalMoneyEarned());
        vanderhorn.payout(vanderhorn.getSalary());
        System.out.println("GHS has spent for salary to " + vanderhorn.getName()
                +" and now has $" + school.getTotalMoneyEarned());
        System.out.println(mateusz);
        melissa.payout(melissa.getSalary());
        System.out.println(melissa);
    }
}
