package school.management.system;

import java.util.List;

/**
 * Many teachers, many students.
 * Implements teachers and student
 * using an ArrayList.
 */
public class School {
    private String name;
    private List<Teacher> teachers;
    private List<Student> students;
    private static int  totalMoneyEarned;
    private static int totalMoneySpent;

    /**
     * new school object is created.
     * @param teachers list of teachers in the school.
     * @param students list of students int the school.
     */
    public School(String name, List<Teacher> teachers, List<Student> students) {
        this.name = name;
        this.teachers = teachers;
        this.students = students;
        totalMoneyEarned=0;
        totalMoneySpent=0;
    }

    public void addTeacher(Teacher teacher) {
        teachers.add(teacher);
    }

    public List<Teacher> getTeachers() {
        return teachers;
    }

    public void addStudent(Student student) {
        students.add(student);
    }

    //TODO: 6) create a method to expel a student and reimburse the loans he payed until now.

    public List<Student> getStudents() {
        return students;
    }

    public int getTotalMoneyEarned() {
        return totalMoneyEarned;
    }

    public int getTotalMoneySpent() {
        return totalMoneySpent;
    }

    /**
     * Adds the total money earned by the school.
     * @param moneyEarned money that is supposed to be added.
     */
    public static void updateIncomes(int moneyEarned) {
        totalMoneyEarned += moneyEarned;
    }

    /**
     * Update the money that is spent by the school which
     * is the salary given by the school to its teachers.
     * @param moneySpent the money spent by school.
     */
    public static void updateExpenses(int moneySpent) {
        totalMoneyEarned -= moneySpent;
     }

    @Override
    public String toString() {
        return "<" + name + "> teachers: " + teachers.size() +
                ", students: " + students.size();        //TODO: 3) add sum of loans payed by every student
    }
}
